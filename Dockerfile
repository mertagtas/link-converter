FROM adoptopenjdk/openjdk11-openj9:alpine

RUN mkdir /opt/app

COPY build/libs/link-converter-*.jar link-converter.jar

EXPOSE 8080

ENV JAVA_OPTS="-Xmx1024m"

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar link-converter.jar" ]
