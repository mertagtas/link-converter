# Trendyol Link Converter Service

## Dependencies

* JDK 11
* Docker, docker-compose

## How to run

```sh
# build project
$ ./gradlew clean build
# test covarage report => build/jacocoHtml

# Build docker images and run docker containers
# Go to docker folder in resources
$ cd src/main/resources/docker

# Build and run couchbase db container
$ docker-compose up -d --build db

# Check db logs 
$ docker-compose logs -f db 

# Go to http://localhost:8091/ui/index.html to login couchbase
# username: admin, password: password

# Build and run app container
$ docker-compose up -d --build app

# Check app logs 
$ docker-compose logs -f app

# Api documentation is accessible via  http://localhost:8080/swagger-ui.html.

```

## Example Http Requests

```http
POST http://localhost:8080/api/deep-links
Accept: */*
Cache-Control: no-cache
Content-Type: application/json

{
  "webUrlLink": "https://www.trendyol.com/brand/name-p-12312313?boutiqueId=12312&merchantId=1231231"
}

###

POST http://localhost:8080/api/deep-links
Accept: */*
Cache-Control: no-cache
Content-Type: application/json

{
  "webUrlLink": "https://www.trendyol.com/tum--urunler?q=asdasdsd"
}

###


POST http://localhost:8080/api/deep-links
Accept: */*
Cache-Control: no-cache
Content-Type: application/json

{
  "webUrlLink": "https://www.trendyol.com/Hesabim/Favoriler"
}

###

POST http://localhost:8080/api/web-urls
Accept: */*
Cache-Control: no-cache
Content-Type: application/json

{
  "deepLink": "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064"
}

###

POST http://localhost:8080/api/web-urls
Accept: */*
Cache-Control: no-cache
Content-Type: application/json

{
  "deepLink": "ty://?Page=Search&Query=%C3%A7anta"
}

###


POST http://localhost:8080/api/web-urls
Accept: */*
Cache-Control: no-cache
Content-Type: application/json

{
  "deepLink": "ty://?Page=Orders"
}

###

```