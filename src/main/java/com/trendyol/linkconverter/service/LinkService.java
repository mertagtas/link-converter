package com.trendyol.linkconverter.service;

import com.trendyol.linkconverter.storage.LinkConversionStorageService;

public class LinkService {

    private final LinkConverter linkConverter;
    private final LinkConversionStorageService linkConversionStorageService;

    public LinkService(LinkConverter linkConverter, LinkConversionStorageService linkConversionStorageService) {
        this.linkConverter = linkConverter;
        this.linkConversionStorageService = linkConversionStorageService;
    }

    public String convert(String link) {
        String convertedLink = linkConverter.convert(link);
        linkConversionStorageService.save(link, convertedLink);
        return convertedLink;
    }

}
