package com.trendyol.linkconverter.service;

import com.trendyol.linkconverter.exception.ApplicableResolverNotFound;
import com.trendyol.linkconverter.resolver.Resolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class LinkConverter {

    private static Logger logger = LoggerFactory.getLogger(LinkConverter.class);

    private final List<Resolver> linkRequestResolvers;

    public LinkConverter(List<Resolver> linkRequestResolvers) {
        this.linkRequestResolvers = linkRequestResolvers;
    }

    public String convert(String link) {
        Resolver linkRequestResolver = linkRequestResolvers
                .stream()
                .filter(requestResolver -> requestResolver.canResolve(link))
                .findFirst()
                .orElseThrow(() -> new ApplicableResolverNotFound(link + " link is not convertible."));
        logger.info("{} link is resolvable by {} ", link, linkRequestResolver.getClass());
        return linkRequestResolver.resolve(link);
    }
}
