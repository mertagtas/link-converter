package com.trendyol.linkconverter.resolver.weburl;

import com.trendyol.linkconverter.builder.ProductPageWebUrlBuilder;
import com.trendyol.linkconverter.exception.RegexDismatchException;
import com.trendyol.linkconverter.resolver.Resolver;
import com.trendyol.linkconverter.util.RegexUtil;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class WebUrlProductPageResolver implements Resolver {

    private static final Pattern PRODUCT_PAGE_PATTERN = Pattern.compile("ty://\\?Page=Product&ContentId=([0-9]+)(&?([^#]*))?");

    @Override
    public boolean canResolve(String webUrlLink) {
        return PRODUCT_PAGE_PATTERN.matcher(webUrlLink).matches();
    }

    @Override
    public String resolve(String webUrlLink) {
        Matcher matcher = PRODUCT_PAGE_PATTERN.matcher(webUrlLink);
        if (matcher.matches()) {
            String contentId = matcher.group(1);
            String query = matcher.group(3);
            Map<String, String> queryParams = RegexUtil.parseQueryParams(query);
            String campaignId = queryParams.get("CampaignId");
            String merchantId = queryParams.get("MerchantId");

            return new ProductPageWebUrlBuilder()
                    .campaignId(campaignId)
                    .contentId(contentId)
                    .merchantId(merchantId)
                    .build();
        } else {
            throw new RegexDismatchException(webUrlLink + " is not a valid product page link");
        }
    }


}
