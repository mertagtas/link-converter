package com.trendyol.linkconverter.resolver.weburl;

import com.trendyol.linkconverter.resolver.Resolver;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class WebUrlOtherPageResolver implements Resolver {

    private static final Pattern OTHER_PAGE_PATTERN = Pattern.compile("ty://\\?Page=.+");
    private static final String HOME_PAGE_LINK = "https://www.trendyol.com";

    @Override
    public boolean canResolve(String webUrlLink) {
        return OTHER_PAGE_PATTERN.matcher(webUrlLink).matches();
    }

    @Override
    public String resolve(String webUrlLink) {
        return HOME_PAGE_LINK;
    }

}
