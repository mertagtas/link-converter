package com.trendyol.linkconverter.resolver;

public interface Resolver {
    boolean canResolve(String t);

    String resolve(String t);
}
