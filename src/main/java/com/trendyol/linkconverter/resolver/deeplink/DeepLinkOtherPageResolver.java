package com.trendyol.linkconverter.resolver.deeplink;

import com.trendyol.linkconverter.resolver.Resolver;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class DeepLinkOtherPageResolver implements Resolver {

    private static final Pattern OTHER_PAGE_PATTERN = Pattern.compile("https:\\/\\/www\\.trendyol\\.com.*");
    private static final String HOME_PAGE_LINK = "ty://?Page=Home";

    @Override
    public boolean canResolve(String webUrlLink) {
        return OTHER_PAGE_PATTERN.matcher(webUrlLink).matches();
    }

    @Override
    public String resolve(String webUrlLink) {
        return HOME_PAGE_LINK;
    }

}
