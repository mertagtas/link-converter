package com.trendyol.linkconverter.resolver.deeplink;

import com.trendyol.linkconverter.builder.ProductPageDeepLinkBuilder;
import com.trendyol.linkconverter.exception.RegexDismatchException;
import com.trendyol.linkconverter.resolver.Resolver;
import com.trendyol.linkconverter.util.RegexUtil;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class DeepLinkProductPageResolver implements Resolver {

    private static final Pattern PRODUCT_PAGE_PATTERN = Pattern.compile("https:\\/\\/www\\.trendyol\\.com\\/.+\\/.+-p-([0-9]+)(\\?([^#]*))?");

    @Override
    public boolean canResolve(String deepLink) {
        return PRODUCT_PAGE_PATTERN.matcher(deepLink).matches();
    }

    @Override
    public String resolve(String deepLink) {
        Matcher matcher = PRODUCT_PAGE_PATTERN.matcher(deepLink);
        if (matcher.matches()) {
            String contentId = matcher.group(1);
            String query = matcher.group(3);
            Map<String, String> queryParams = RegexUtil.parseQueryParams(query);
            String boutiqueId = queryParams.get("boutiqueId");
            String merchantId = queryParams.get("merchantId");

            return new ProductPageDeepLinkBuilder()
                    .boutiqueId(boutiqueId)
                    .contentId(contentId)
                    .merchantId(merchantId)
                    .build();
        } else {
            throw new RegexDismatchException(deepLink + " is not a valid product page link");
        }
    }

}
