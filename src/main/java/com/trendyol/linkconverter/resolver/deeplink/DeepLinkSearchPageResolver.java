package com.trendyol.linkconverter.resolver.deeplink;

import com.trendyol.linkconverter.exception.RegexDismatchException;
import com.trendyol.linkconverter.resolver.Resolver;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class DeepLinkSearchPageResolver implements Resolver {

    private static final Pattern SEARCH_PAGE_PATTERN = Pattern.compile("https:\\/\\/www\\.trendyol\\.com\\/tum--urunler\\?q=(.+)");

    private static final String SEARCH_PAGE_LINK = "ty://?Page=Search&Query=";

    @Override
    public boolean canResolve(String webUrlLink) {
        return SEARCH_PAGE_PATTERN.matcher(webUrlLink).matches();
    }

    @Override
    public String resolve(String webUrlLink) {
        Matcher matcher = SEARCH_PAGE_PATTERN.matcher(webUrlLink);
        if (matcher.matches()) {
            String query = matcher.group(1);
            return SEARCH_PAGE_LINK + query;
        } else {
            throw new RegexDismatchException(webUrlLink + " is not a valid search page link");
        }
    }

}
