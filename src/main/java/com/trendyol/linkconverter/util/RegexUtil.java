package com.trendyol.linkconverter.util;

import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {
    private static final Pattern QUERY_PARAM_PATTERN = Pattern.compile("([^&=]+)(=?)([^&]+)?");

    private RegexUtil() {
    }

    public static Map<String, String> parseQueryParams(String query) {
        if (StringUtils.isEmpty(query)) {
            return new HashMap<>();
        }
        Map<String, String> queryParamMap = new HashMap<>();
        Matcher matcher = QUERY_PARAM_PATTERN.matcher(query);
        while (matcher.find()) {
            String name = matcher.group(1);
            String eq = matcher.group(2);
            String value = matcher.group(3);
            queryParamMap.put(name, (value != null ? value : (StringUtils.hasLength(eq) ? "" : null)));
        }
        return queryParamMap;
    }
}
