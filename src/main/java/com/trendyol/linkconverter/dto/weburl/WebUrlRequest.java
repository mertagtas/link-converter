package com.trendyol.linkconverter.dto.weburl;

import javax.validation.constraints.NotBlank;

public class WebUrlRequest {

    @NotBlank
    private String deepLink;

    public String getDeepLink() {
        return deepLink;
    }

    public WebUrlRequest setDeepLink(String deepLink) {
        this.deepLink = deepLink;
        return this;
    }

    @Override
    public String toString() {
        return "WebUrlRequest{" +
                "deepLink='" + deepLink + '\'' +
                '}';
    }
}

