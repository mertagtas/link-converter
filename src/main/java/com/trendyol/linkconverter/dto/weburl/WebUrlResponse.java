package com.trendyol.linkconverter.dto.weburl;

import javax.validation.constraints.NotNull;

public class WebUrlResponse {

    @NotNull
    private String webUrl;

    public String getWebUrl() {
        return webUrl;
    }

    public WebUrlResponse setWebUrl(String webUrl) {
        this.webUrl = webUrl;
        return this;
    }
}

