package com.trendyol.linkconverter.dto;

import javax.validation.constraints.NotNull;


public class ErrorResponse {

    private String timestamp;
    private Integer status;
    private String error;
    private String message;
    private String path;

    @NotNull
    public String getTimestamp() {
        return timestamp;
    }

    public ErrorResponse setTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @NotNull
    public Integer getStatus() {
        return status;
    }

    public ErrorResponse setStatus(Integer status) {
        this.status = status;
        return this;
    }

    @NotNull
    public String getError() {
        return error;
    }

    public ErrorResponse setError(String error) {
        this.error = error;
        return this;
    }

    @NotNull
    public String getMessage() {
        return message;
    }

    public ErrorResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    @NotNull
    public String getPath() {
        return path;
    }

    public ErrorResponse setPath(String path) {
        this.path = path;
        return this;
    }
}

