package com.trendyol.linkconverter.dto.deeplink;

import javax.validation.constraints.NotBlank;

public class DeepLinkRequest {

    @NotBlank
    private String webUrlLink;

    public String getWebUrlLink() {
        return webUrlLink;
    }

    public DeepLinkRequest setWebUrlLink(String webUrlLink) {
        this.webUrlLink = webUrlLink;
        return this;
    }

    @Override
    public String toString() {
        return "DeepLinkRequest{" +
                "webUrlLink='" + webUrlLink + '\'' +
                '}';
    }
}

