package com.trendyol.linkconverter.dto.deeplink;


import javax.validation.constraints.NotNull;

public class DeepLinkResponse {

    @NotNull
    private String deepLink;

    public String getDeepLink() {
        return deepLink;
    }

    public DeepLinkResponse setDeepLink(String deepLink) {
        this.deepLink = deepLink;
        return this;
    }

    @Override
    public String toString() {
        return "DeepLinkResponse{" +
                "deepLink='" + deepLink + '\'' +
                '}';
    }
}

