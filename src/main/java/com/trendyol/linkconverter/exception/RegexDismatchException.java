package com.trendyol.linkconverter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class RegexDismatchException extends IllegalArgumentException {

    public RegexDismatchException(String message) {
        super(message);
    }

}
