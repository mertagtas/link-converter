package com.trendyol.linkconverter.builder;

import org.springframework.util.StringUtils;

public class ProductPageWebUrlBuilder {

    public static final String WEB_URL = "https://www.trendyol.com/brand/name-p-";
    public static final String BOUTIQUE_ID = "boutiqueId=";
    public static final String MERCHANT_ID = "merchantId=";

    private String contentId;
    private String campaignId;
    private String merchantId;

    public ProductPageWebUrlBuilder contentId(String contentId) {
        this.contentId = contentId;
        return this;
    }

    public ProductPageWebUrlBuilder campaignId(String campaignId) {
        this.campaignId = campaignId;
        return this;
    }

    public ProductPageWebUrlBuilder merchantId(String merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public String build() {
        StringBuilder webUrlStringBuilder = new StringBuilder();
        webUrlStringBuilder.append(WEB_URL).append(contentId);

        String queryUrl = getQueryUrl();
        if (!StringUtils.isEmpty(queryUrl)) {
            webUrlStringBuilder.append("?").append(queryUrl);
        }
        return webUrlStringBuilder.toString();
    }

    private String getQueryUrl() {
        StringBuilder queryParamStringBuilder = new StringBuilder();
        if (!StringUtils.isEmpty(campaignId)) {
            queryParamStringBuilder.append(BOUTIQUE_ID).append(campaignId);
        }
        if (!StringUtils.isEmpty(merchantId)) {
            if (queryParamStringBuilder.length() > 0) {
                queryParamStringBuilder.append("&");
            }
            queryParamStringBuilder.append(MERCHANT_ID).append(merchantId);
        }
        return queryParamStringBuilder.toString();
    }
}
