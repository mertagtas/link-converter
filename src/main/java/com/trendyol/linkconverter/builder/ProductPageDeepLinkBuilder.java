package com.trendyol.linkconverter.builder;

import org.springframework.util.StringUtils;

public class ProductPageDeepLinkBuilder {

    public static final String DEEP_LINK = "ty://?";
    public static final String PRODUCT_PAGE = "Page=Product";
    public static final String CONTENT_ID = "&ContentId=";
    public static final String MERCHANT_ID = "&MerchantId=";
    public static final String CAMPAIGN_ID = "&CampaignId=";

    private String contentId;
    private String boutiqueId;
    private String merchantId;

    public ProductPageDeepLinkBuilder contentId(String contentId) {
        this.contentId = contentId;
        return this;
    }

    public ProductPageDeepLinkBuilder boutiqueId(String boutiqueId) {
        this.boutiqueId = boutiqueId;
        return this;
    }

    public ProductPageDeepLinkBuilder merchantId(String merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public String build() {
        StringBuilder deepLinkBuilder = new StringBuilder();
        deepLinkBuilder.append(DEEP_LINK).append(PRODUCT_PAGE).append(CONTENT_ID).append(contentId);
        if (!StringUtils.isEmpty(boutiqueId)) {
            deepLinkBuilder.append(CAMPAIGN_ID).append(boutiqueId);
        }
        if (!StringUtils.isEmpty(merchantId)) {
            deepLinkBuilder.append(MERCHANT_ID).append(merchantId);
        }
        return deepLinkBuilder.toString();
    }
}
