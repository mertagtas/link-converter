package com.trendyol.linkconverter;

import com.trendyol.linkconverter.resolver.Resolver;
import com.trendyol.linkconverter.resolver.deeplink.DeepLinkOtherPageResolver;
import com.trendyol.linkconverter.resolver.deeplink.DeepLinkProductPageResolver;
import com.trendyol.linkconverter.resolver.deeplink.DeepLinkSearchPageResolver;
import com.trendyol.linkconverter.resolver.weburl.WebUrlOtherPageResolver;
import com.trendyol.linkconverter.resolver.weburl.WebUrlProductPageResolver;
import com.trendyol.linkconverter.resolver.weburl.WebUrlSearchPageResolver;
import com.trendyol.linkconverter.service.LinkConverter;
import com.trendyol.linkconverter.service.LinkService;
import com.trendyol.linkconverter.storage.LinkConversionStorageService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public List<Resolver> deepLinkRequestResolvers(
            final DeepLinkProductPageResolver deepLinkProductPageResolver,
            final DeepLinkSearchPageResolver deepLinkSearchPageResolver,
            final DeepLinkOtherPageResolver deepLinkOtherPageResolver) {
        return List.of(deepLinkProductPageResolver, deepLinkSearchPageResolver, deepLinkOtherPageResolver);
    }

    @Bean
    public LinkConverter deepLinkConverter(@Qualifier("deepLinkRequestResolvers") final List<Resolver> deepLinkRequestResolvers) {
        return new LinkConverter(deepLinkRequestResolvers);
    }

    @Bean
    public LinkService deepLinkService(final LinkConverter deepLinkConverter, final LinkConversionStorageService linkConversionStorageService) {
        return new LinkService(deepLinkConverter, linkConversionStorageService);
    }

    @Bean
    public List<Resolver> webUrlRequestResolvers(
            final WebUrlProductPageResolver webUrlProductPageResolver,
            final WebUrlOtherPageResolver webUrlOtherPageResolver,
            final WebUrlSearchPageResolver webUrlSearchPageResolver) {
        return List.of(webUrlProductPageResolver, webUrlSearchPageResolver, webUrlOtherPageResolver);
    }

    @Bean
    public LinkConverter webUrlConverter(@Qualifier("webUrlRequestResolvers") final List<Resolver> webUrlRequestResolvers) {
        return new LinkConverter(webUrlRequestResolvers);
    }

    @Bean
    public LinkService webUrlLinkService(final LinkConverter webUrlConverter, final LinkConversionStorageService linkConversionStorageService) {
        return new LinkService(webUrlConverter, linkConversionStorageService);
    }

}
