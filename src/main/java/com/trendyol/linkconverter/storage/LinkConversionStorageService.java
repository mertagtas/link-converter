package com.trendyol.linkconverter.storage;

import org.springframework.stereotype.Service;

@Service
public class LinkConversionStorageService {

    private final LinkConversionRepository linkConversionRepository;
    private final IdGenerator idGenerator;

    public LinkConversionStorageService(LinkConversionRepository linkConversionRepository, IdGenerator idGenerator) {
        this.linkConversionRepository = linkConversionRepository;
        this.idGenerator = idGenerator;
    }

    public LinkConversion save(String request, String response) {
        return linkConversionRepository.save(
                new LinkConversion()
                        .setId(idGenerator.generate())
                        .setRequest(request)
                        .setResponse(response)
        );
    }
}
