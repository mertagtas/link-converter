package com.trendyol.linkconverter.storage;

import org.springframework.data.couchbase.repository.CouchbaseRepository;

public interface LinkConversionRepository extends CouchbaseRepository<LinkConversion, String> {
}
