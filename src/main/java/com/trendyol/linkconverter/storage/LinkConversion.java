package com.trendyol.linkconverter.storage;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

import javax.validation.constraints.NotNull;

@Document
public class LinkConversion {

    @Id
    private String id;

    @Field
    @NotNull
    private String request;

    @Field
    @NotNull
    private String response;

    public String getId() {
        return id;
    }

    public LinkConversion setId(String id) {
        this.id = id;
        return this;
    }

    public String getRequest() {
        return request;
    }

    public LinkConversion setRequest(String request) {
        this.request = request;
        return this;
    }

    public String getResponse() {
        return response;
    }

    public LinkConversion setResponse(String response) {
        this.response = response;
        return this;
    }
}
