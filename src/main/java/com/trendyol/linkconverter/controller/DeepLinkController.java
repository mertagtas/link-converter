package com.trendyol.linkconverter.controller;

import com.trendyol.linkconverter.dto.ErrorResponse;
import com.trendyol.linkconverter.dto.deeplink.DeepLinkRequest;
import com.trendyol.linkconverter.dto.deeplink.DeepLinkResponse;
import com.trendyol.linkconverter.service.LinkService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Validated
@Tag(name = "deeplink", description = "DeepLink API")
public class DeepLinkController {

    private static Logger logger = LoggerFactory.getLogger(DeepLinkController.class);

    private final LinkService deepLinkService;

    public DeepLinkController(LinkService deepLinkService) {
        this.deepLinkService = deepLinkService;
    }

    @Operation(summary = "Converts to deeplink", description = "Converts web url link to deep link")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Web url link is converted to deep link, successfully",
                    content = @Content(schema = @Schema(implementation = DeepLinkResponse.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request",
                    content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @RequestMapping(value = "/api/deep-links",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    public ResponseEntity<DeepLinkResponse> convertToDeepLink(@Valid @RequestBody DeepLinkRequest deepLinkRequest) {
        String deepLink = deepLinkService.convert(deepLinkRequest.getWebUrlLink());
        DeepLinkResponse deepLinkResponse = new DeepLinkResponse().setDeepLink(deepLink);
        logger.info("WebUrl to deep link request {} -> {}", deepLinkRequest, deepLinkResponse);
        return ResponseEntity.ok(deepLinkResponse);
    }

}
