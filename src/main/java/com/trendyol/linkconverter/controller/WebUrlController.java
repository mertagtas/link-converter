package com.trendyol.linkconverter.controller;

import com.trendyol.linkconverter.dto.ErrorResponse;
import com.trendyol.linkconverter.dto.weburl.WebUrlRequest;
import com.trendyol.linkconverter.dto.weburl.WebUrlResponse;
import com.trendyol.linkconverter.service.LinkService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Validated
@Tag(name = "weburl", description = "WebUrl API")
public class WebUrlController {

    private static Logger logger = LoggerFactory.getLogger(WebUrlController.class);

    private final LinkService webUrlLinkService;

    public WebUrlController(LinkService webUrlLinkService) {
        this.webUrlLinkService = webUrlLinkService;
    }

    @Operation(summary = "Converts to weburl", description = "Converts deep link to web url")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deep link is converted to web url, successfully",
                    content = @Content(schema = @Schema(implementation = WebUrlResponse.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request",
                    content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @RequestMapping(value = "/api/web-urls",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    public ResponseEntity<WebUrlResponse> convertToWebUrl(@Valid @RequestBody WebUrlRequest webUrlRequest) {
        String webUrl = webUrlLinkService.convert(webUrlRequest.getDeepLink());
        WebUrlResponse webUrlResponse = new WebUrlResponse().setWebUrl(webUrl);
        logger.info("Deep link to web url request {} -> {}", webUrlRequest, webUrlResponse);
        return ResponseEntity.ok(webUrlResponse);
    }

}
