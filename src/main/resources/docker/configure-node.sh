set -m

/entrypoint.sh couchbase-server &

sleep 10

# Setup initial cluster/ Initialize Node
couchbase-cli cluster-init -c 127.0.0.1 --cluster-name trendyol --cluster-username admin \
--cluster-password password --services data,index,query,fts --cluster-ramsize 256 --cluster-index-ramsize 256 \
--cluster-fts-ramsize 256 --index-storage-setting default \

curl -v http://127.0.0.1:8091/settings/web -d port=8091 -d username=admin -d password=password

sleep 3

# Setup Bucket
couchbase-cli bucket-create -c 127.0.0.1:8091 --username admin \
--password password  --bucket trendyol --bucket-type couchbase \
--bucket-ramsize 256


sleep 3

cbq -u admin -p password -s "CREATE PRIMARY INDEX idx_primary ON \`trendyol\`;"

sleep 3

echo "configure-node.sh is done"

fg 1