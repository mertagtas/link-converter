package com.trendyol.linkconverter.resolver.weburl;

import com.trendyol.linkconverter.dummy.ProductPageLinkArgumentsProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class WebUrlProductPageResolverTest {

    private WebUrlProductPageResolver webUrlProductPageResolver = new WebUrlProductPageResolver();

    @ParameterizedTest
    @ArgumentsSource(ProductPageLinkArgumentsProvider.class)
    public void testIfProductPagesAreConvertedToExpectedDeepLinks(String webUrlLink, String deepLink) {
        Assertions.assertTrue(webUrlProductPageResolver.canResolve(deepLink));
        Assertions.assertEquals(webUrlProductPageResolver.resolve(deepLink), webUrlLink);
    }

    @Test
    public void shouldNotResolveIfNoContentId() {
        Assertions.assertFalse(webUrlProductPageResolver.canResolve("ty://?Page=Product&ContentId="));
        Assertions.assertFalse(webUrlProductPageResolver.canResolve("ty://?Page=Product"));
    }

    @Test
    public void shouldNotResolveIfBaseDeepLinkIsWrong() {
        Assertions.assertFalse(webUrlProductPageResolver.canResolve("ty1://?Page=Product&ContentId=123123"));
    }

}
