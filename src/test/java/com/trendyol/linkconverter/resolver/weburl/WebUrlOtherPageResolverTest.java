package com.trendyol.linkconverter.resolver.weburl;

import com.trendyol.linkconverter.dummy.OtherPageWebUrlArgumentsProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class WebUrlOtherPageResolverTest {

    private WebUrlOtherPageResolver webUrlOtherPageResolver = new WebUrlOtherPageResolver();

    @ParameterizedTest
    @ArgumentsSource(OtherPageWebUrlArgumentsProvider.class)
    public void testIfOtherPagesAreConvertedToExpectedDeepLinks(String webUrlLink, String deepLink) {
        Assertions.assertTrue(webUrlOtherPageResolver.canResolve(deepLink));
        Assertions.assertEquals(webUrlOtherPageResolver.resolve(deepLink), webUrlLink);
    }

    @Test
    public void shouldNotResolveIfBaseDeepLinkIsWrong() {
        Assertions.assertFalse(webUrlOtherPageResolver.canResolve("ty1://?Page=Product&ContentId=123123"));
    }

}
