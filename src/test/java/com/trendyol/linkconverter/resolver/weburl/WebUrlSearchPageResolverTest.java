package com.trendyol.linkconverter.resolver.weburl;

import com.trendyol.linkconverter.dummy.SearchPageLinkArgumentsProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class WebUrlSearchPageResolverTest {

    private WebUrlSearchPageResolver webUrlSearchPageResolver = new WebUrlSearchPageResolver();

    @ParameterizedTest
    @ArgumentsSource(SearchPageLinkArgumentsProvider.class)
    public void testIfSearchPagesAreConvertedToExpectedDeepLinks(String webUrlLink, String deepLink) {
        Assertions.assertTrue(webUrlSearchPageResolver.canResolve(deepLink));
        Assertions.assertEquals(webUrlSearchPageResolver.resolve(deepLink), webUrlLink);
    }

    @Test
    public void shouldConvertQueryWithTurkishCharacters() {
        String queryWithTurkishCharacters = "ty://?Page=Search&Query=çaüğ";
        String expectedWebUrl = "https://www.trendyol.com/tum--urunler?q=çaüğ";
        Assertions.assertTrue(webUrlSearchPageResolver.canResolve(queryWithTurkishCharacters));
        Assertions.assertEquals(expectedWebUrl, webUrlSearchPageResolver.resolve(queryWithTurkishCharacters));
    }

    @Test
    public void shouldNotResolveIfNoQueryParameter() {
        Assertions.assertFalse(webUrlSearchPageResolver.canResolve("ty://?Page=Search&Query="));
    }

    @Test
    public void shouldNotResolveIfNoQueryInUrl() {
        Assertions.assertFalse(webUrlSearchPageResolver.canResolve("ty://?Page=Search"));
    }

    @Test
    public void shouldNotResolveIfBaseDeepLinkIsWrong() {
        Assertions.assertFalse(webUrlSearchPageResolver.canResolve("ty1://?Page=Product&ContentId=123123"));
    }
}
