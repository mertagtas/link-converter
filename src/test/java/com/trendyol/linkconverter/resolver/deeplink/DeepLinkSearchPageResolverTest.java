package com.trendyol.linkconverter.resolver.deeplink;

import com.trendyol.linkconverter.dummy.SearchPageLinkArgumentsProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class DeepLinkSearchPageResolverTest {

    private DeepLinkSearchPageResolver deepLinkSearchPageResolver = new DeepLinkSearchPageResolver();

    @ParameterizedTest
    @ArgumentsSource(SearchPageLinkArgumentsProvider.class)
    public void testIfSearchPagesAreConvertedToExpectedDeepLinks(String webUrlLink, String deepLink) {
        Assertions.assertTrue(deepLinkSearchPageResolver.canResolve(webUrlLink));
        Assertions.assertEquals(deepLinkSearchPageResolver.resolve(webUrlLink), deepLink);
    }

    @Test
    public void shouldConvertQueryWithTurkishCharacters() {
        String queryWithTurkishCharacters = "https://www.trendyol.com/tum--urunler?q=çaüğ";
        String expectedDeepLink = "ty://?Page=Search&Query=çaüğ";
        Assertions.assertTrue(deepLinkSearchPageResolver.canResolve(queryWithTurkishCharacters));
        Assertions.assertEquals(deepLinkSearchPageResolver.resolve(queryWithTurkishCharacters), expectedDeepLink);
    }

    @Test
    public void shouldNotResolveIfNoQueryParameter() {
        Assertions.assertFalse(deepLinkSearchPageResolver.canResolve("https://www.trendyol.com/tum--urunler"));
    }

    @Test
    public void shouldNotResolveIfNoTumUrunlerInUrl() {
        Assertions.assertFalse(deepLinkSearchPageResolver.canResolve("https://www.trendyol.com/tum-urunler"));
        Assertions.assertFalse(deepLinkSearchPageResolver.canResolve("https://www.trendyol.com/"));
    }

    @Test
    public void shouldNotResolveIfBaseUrlIsWrong() {
        Assertions.assertFalse(deepLinkSearchPageResolver.canResolve("https://trendyol1.com/tum--urunler?q=weq"));
        Assertions.assertFalse(deepLinkSearchPageResolver.canResolve("https://wwww.trendyol1.com/tum--urunler?q=weq"));
        Assertions.assertFalse(deepLinkSearchPageResolver.canResolve("https://trendyo.com/tum--urunler?q=weq"));
    }
}
