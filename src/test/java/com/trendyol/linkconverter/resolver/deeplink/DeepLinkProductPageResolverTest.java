package com.trendyol.linkconverter.resolver.deeplink;

import com.trendyol.linkconverter.dummy.ProductPageLinkArgumentsProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class DeepLinkProductPageResolverTest {

    private DeepLinkProductPageResolver deepLinkProductPageResolver = new DeepLinkProductPageResolver();

    @ParameterizedTest
    @ArgumentsSource(ProductPageLinkArgumentsProvider.class)
    public void testIfProductPagesAreConvertedToExpectedDeepLinks(String webUrlLink, String deepLink) {
        Assertions.assertTrue(deepLinkProductPageResolver.canResolve(webUrlLink));
        Assertions.assertEquals(deepLinkProductPageResolver.resolve(webUrlLink), deepLink);
    }

    @Test
    public void shouldNotResolveIfNoPInSecondPathSegment() {
        Assertions.assertFalse(deepLinkProductPageResolver.canResolve("https://www.trendyol.com/casio/saat-p1925865"));
    }

    @Test
    public void shouldNotResolveIfNoContentId() {
        Assertions.assertFalse(deepLinkProductPageResolver.canResolve("https://www.trendyol.com/casio/saat-p-"));
    }

    @Test
    public void shouldNotResolveIfBaseUrlIsWrong() {
        Assertions.assertFalse(deepLinkProductPageResolver.canResolve("https://trendyol1.com/casio/saat-p-1925865"));
        Assertions.assertFalse(deepLinkProductPageResolver.canResolve("https://wwww.trendyol1.com/casio/saat-p-1925865"));
        Assertions.assertFalse(deepLinkProductPageResolver.canResolve("https://trendyo.com/casio/saat-p-1925865"));
    }
}
