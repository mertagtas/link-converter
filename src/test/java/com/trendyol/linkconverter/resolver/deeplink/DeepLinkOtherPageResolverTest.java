package com.trendyol.linkconverter.resolver.deeplink;

import com.trendyol.linkconverter.dummy.OtherPageDeepLinkArgumentsProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class DeepLinkOtherPageResolverTest {

    private DeepLinkOtherPageResolver deepLinkOtherPageResolver = new DeepLinkOtherPageResolver();

    @ParameterizedTest
    @ArgumentsSource(OtherPageDeepLinkArgumentsProvider.class)
    public void testIfOtherPagesAreConvertedToExpectedDeepLinks(String webUrlLink, String deepLink) {
        Assertions.assertTrue(deepLinkOtherPageResolver.canResolve(webUrlLink));
        Assertions.assertEquals(deepLinkOtherPageResolver.resolve(webUrlLink), deepLink);
    }

    @Test
    public void shouldNotResolveIfBaseUrlIsWrong() {
        Assertions.assertFalse(deepLinkOtherPageResolver.canResolve("https://trendyol1.com/"));
        Assertions.assertFalse(deepLinkOtherPageResolver.canResolve("https://wwww.trendyol1.com"));
        Assertions.assertFalse(deepLinkOtherPageResolver.canResolve("https://trendyo.com"));
    }

}
