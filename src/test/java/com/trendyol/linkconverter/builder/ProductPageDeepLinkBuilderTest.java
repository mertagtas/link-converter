package com.trendyol.linkconverter.builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ProductPageDeepLinkBuilderTest {

    @Test
    public void shouldBuildProductDeepLink() {
        assertEquals("ty://?Page=Product&ContentId=1233", new ProductPageDeepLinkBuilder().contentId("1233").build());
        assertEquals("ty://?Page=Product&ContentId=1233&MerchantId=asd", new ProductPageDeepLinkBuilder().contentId("1233").merchantId("asd").build());
        assertEquals("ty://?Page=Product&ContentId=1233&CampaignId=bouti123", new ProductPageDeepLinkBuilder().contentId("1233").boutiqueId("bouti123").build());
    }
}
