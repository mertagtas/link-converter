package com.trendyol.linkconverter.builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ProductPageWebUrlBuilderTest {

    @Test
    public void shouldBuildProductDeepLink() {
        assertEquals("https://www.trendyol.com/brand/name-p-1925865", new ProductPageWebUrlBuilder().contentId("1925865").build());
        assertEquals("https://www.trendyol.com/brand/name-p-1925865?merchantId=105064", new ProductPageWebUrlBuilder().contentId("1925865").merchantId("105064").build());
        assertEquals("https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892", new ProductPageWebUrlBuilder().contentId("1925865").campaignId("439892").build());
        assertEquals("https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892&merchantId=105064", new ProductPageWebUrlBuilder().contentId("1925865").campaignId("439892").merchantId("105064").build());
    }
}
