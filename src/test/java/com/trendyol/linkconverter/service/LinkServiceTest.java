package com.trendyol.linkconverter.service;

import com.trendyol.linkconverter.exception.ApplicableResolverNotFound;
import com.trendyol.linkconverter.storage.LinkConversionStorageService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LinkServiceTest {

    private static final String WEB_URL_LINK = "https://www.trendyol.com/";
    private static final String DEEP_LINK = "ty://?Page=Home";

    private final LinkConverter linkConverter = mock(LinkConverter.class);
    private final LinkConversionStorageService linkConversionStorageService = mock(LinkConversionStorageService.class);

    @Test
    public void shouldConvertToDeepLink() {
        LinkService linkService = new LinkService(linkConverter, linkConversionStorageService);
        when(linkConverter.convert(WEB_URL_LINK)).thenReturn(DEEP_LINK);
        String deepLink = linkService.convert(WEB_URL_LINK);
        verify(linkConversionStorageService, times(1)).save(WEB_URL_LINK, DEEP_LINK);
        assertEquals(DEEP_LINK, deepLink);
    }

    @Test
    public void shouldFailIfLinkIsNotResolvable() {
        LinkService linkService = new LinkService(linkConverter, linkConversionStorageService);
        when(linkConverter.convert(WEB_URL_LINK)).thenThrow(new ApplicableResolverNotFound());
        assertThrows(ApplicableResolverNotFound.class, () -> linkService.convert(WEB_URL_LINK));
    }

}
