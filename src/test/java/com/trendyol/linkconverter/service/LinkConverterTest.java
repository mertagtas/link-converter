package com.trendyol.linkconverter.service;

import com.trendyol.linkconverter.exception.ApplicableResolverNotFound;
import com.trendyol.linkconverter.resolver.Resolver;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LinkConverterTest {

    private static final String TEST_LINK = "https://www.trendyol.com/";

    @Test
    public void shouldThrowExceptionIfNoApplicableResolver() {
        LinkConverter linkConverter = new LinkConverter(List.of());
        assertThrows(ApplicableResolverNotFound.class, () -> linkConverter.convert(TEST_LINK));
    }

    @Test
    public void shouldThrowExceptionIfItIsNotResolvable() {
        Resolver testResolver = mock(Resolver.class);
        when(testResolver.canResolve(TEST_LINK)).thenReturn(false);
        LinkConverter linkConverter = new LinkConverter(List.of(testResolver));
        assertThrows(ApplicableResolverNotFound.class, () -> linkConverter.convert(TEST_LINK));
    }

    @Test
    public void shouldResolveIfItIsResolvable() {
        Resolver testResolver = mock(Resolver.class);
        when(testResolver.canResolve(TEST_LINK)).thenReturn(true);
        LinkConverter linkConverter = new LinkConverter(List.of(testResolver));
        linkConverter.convert(TEST_LINK);
        verify(testResolver, times(1)).resolve(TEST_LINK);
    }


}
