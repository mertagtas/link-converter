package com.trendyol.linkconverter.dummy;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class OtherPageDeepLinkArgumentsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(
                Arguments.of(
                        "https://www.trendyol.com/Hesabim/Favoriler",
                        "ty://?Page=Home"
                ),
                Arguments.of(
                        "https://www.trendyol.com/Hesabim/#/Siparislerim",
                        "ty://?Page=Home"
                )
        );
    }
}
