package com.trendyol.linkconverter.dummy;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class OtherPageWebUrlArgumentsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(
                Arguments.of(
                        "https://www.trendyol.com",
                        "ty://?Page=Favorites"
                ),
                Arguments.of(
                        "https://www.trendyol.com",
                        "ty://?Page=Orders"
                )
        );
    }
}
