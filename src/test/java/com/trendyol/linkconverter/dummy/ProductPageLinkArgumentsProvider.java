package com.trendyol.linkconverter.dummy;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class ProductPageLinkArgumentsProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(
                Arguments.of(
                        "https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892&merchantId=105064",
                        "ty://?Page=Product&ContentId=1925865&CampaignId=439892&MerchantId=105064"
                ),
                Arguments.of(
                        "https://www.trendyol.com/brand/name-p-1925865",
                        "ty://?Page=Product&ContentId=1925865"
                ),
                Arguments.of(
                        "https://www.trendyol.com/brand/name-p-1925865?boutiqueId=439892",
                        "ty://?Page=Product&ContentId=1925865&CampaignId=439892"
                ),
                Arguments.of(
                        "https://www.trendyol.com/brand/name-p-1925865?merchantId=105064",
                        "ty://?Page=Product&ContentId=1925865&MerchantId=105064"
                )
        );
    }
}
