package com.trendyol.linkconverter.dummy;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class SearchPageLinkArgumentsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.of(
                Arguments.of(
                        "https://www.trendyol.com/tum--urunler?q=elbise",
                        "ty://?Page=Search&Query=elbise"
                ),
                Arguments.of(
                        "https://www.trendyol.com/tum--urunler?q=%C3%A7anta",
                        "ty://?Page=Search&Query=%C3%A7anta"
                ),
                Arguments.of(
                        "https://www.trendyol.com/tum--urunler?q=çanta",
                        "ty://?Page=Search&Query=çanta"
                )
        );
    }
}
