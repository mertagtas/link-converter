package com.trendyol.linkconverter.storage;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LinkConversionStorageServiceTest {

    private LinkConversionRepository linkConversionRepository = mock(LinkConversionRepository.class);
    private IdGenerator idGenerator = mock(IdGenerator.class);

    private static final String RANDOM_ID = "c5af65a2-9f15-4ddd-b5b1-61c1be9e78f2";

    @Test
    public void shouldSaveLinkConversion() {
        final LinkConversion linkConversion = new LinkConversion().setId(RANDOM_ID).setRequest("request").setResponse("response");
        final LinkConversionStorageService linkConversionStorageService = new LinkConversionStorageService(linkConversionRepository, idGenerator);
        when(idGenerator.generate()).thenReturn(RANDOM_ID);
        ArgumentCaptor<LinkConversion> argument = ArgumentCaptor.forClass(LinkConversion.class);
        linkConversionStorageService.save(linkConversion.getRequest(), linkConversion.getResponse());
        verify(linkConversionRepository, times(1)).save(argument.capture());
        assertEquals(RANDOM_ID, argument.getValue().getId());
        assertEquals(linkConversion.getRequest(), argument.getValue().getRequest());
        assertEquals(linkConversion.getResponse(), argument.getValue().getResponse());
    }
}
