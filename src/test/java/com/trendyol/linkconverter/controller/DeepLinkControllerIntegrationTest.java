package com.trendyol.linkconverter.controller;

import com.trendyol.linkconverter.BaseIntegrationTest;
import com.trendyol.linkconverter.dto.deeplink.DeepLinkRequest;
import com.trendyol.linkconverter.dto.deeplink.DeepLinkResponse;
import com.trendyol.linkconverter.dto.ErrorResponse;
import com.trendyol.linkconverter.dummy.OtherPageDeepLinkArgumentsProvider;
import com.trendyol.linkconverter.dummy.ProductPageLinkArgumentsProvider;
import com.trendyol.linkconverter.dummy.SearchPageLinkArgumentsProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DeepLinkControllerIntegrationTest extends BaseIntegrationTest {

    @Autowired
    protected TestRestTemplate restTemplate;

    @LocalServerPort
    private int applicationPort;



    @ParameterizedTest
    @ArgumentsSource(ProductPageLinkArgumentsProvider.class)
    public void shouldConvertCorrectProductPagesToDeepLink(String webUrlLink, String deepLink) {
        DeepLinkRequest deepLinkRequest = new DeepLinkRequest().setWebUrlLink(webUrlLink);
        ResponseEntity<DeepLinkResponse> deepLinkResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/deep-links",
                deepLinkRequest,
                DeepLinkResponse.class);
        assertEquals(deepLinkResponseResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(deepLinkResponseResponseEntity.getBody().getDeepLink(), deepLink);
    }

    @ParameterizedTest
    @ArgumentsSource(SearchPageLinkArgumentsProvider.class)
    public void shouldConvertCorrectSearchPagesToDeepLink(String webUrlLink, String deepLink) {
        DeepLinkRequest deepLinkRequest = new DeepLinkRequest().setWebUrlLink(webUrlLink);
        ResponseEntity<DeepLinkResponse> deepLinkResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/deep-links",
                deepLinkRequest,
                DeepLinkResponse.class);
        assertEquals(deepLinkResponseResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(deepLinkResponseResponseEntity.getBody().getDeepLink(), deepLink);
    }

    @ParameterizedTest
    @ArgumentsSource(OtherPageDeepLinkArgumentsProvider.class)
    public void shouldConvertCorrectOtherPagesToDeepLink(String webUrlLink, String deepLink) {
        DeepLinkRequest deepLinkRequest = new DeepLinkRequest().setWebUrlLink(webUrlLink);
        ResponseEntity<DeepLinkResponse> deepLinkResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/deep-links",
                deepLinkRequest,
                DeepLinkResponse.class);
        assertEquals(deepLinkResponseResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(deepLinkResponseResponseEntity.getBody().getDeepLink(), deepLink);
    }

    @Test
    public void shouldThrowBadRequestIfRequestBodyIsEmpty() {
        DeepLinkRequest deepLinkRequest = new DeepLinkRequest();
        ResponseEntity<ErrorResponse> deepLinkResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/deep-links",
                deepLinkRequest,
                ErrorResponse.class);
        assertEquals(deepLinkResponseResponseEntity.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertTrue(deepLinkResponseResponseEntity.getBody().getMessage().contains("Validation"));
    }

    @Test
    public void shouldThrowBadRequestIfWebUrlLinkIsEmpty() {
        DeepLinkRequest deepLinkRequest = new DeepLinkRequest().setWebUrlLink("");
        ResponseEntity<ErrorResponse> deepLinkResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/deep-links",
                deepLinkRequest,
                ErrorResponse.class);
        assertEquals(deepLinkResponseResponseEntity.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertTrue(deepLinkResponseResponseEntity.getBody().getMessage().contains("Validation"));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "https://www.trendyol1.com/casio/saat-p-",
            "https://trendyol.com/casio/saat-p1925865?boutiqueId=439892&merchantId=105064",
            "http://www.trendyol.com/casio",
            "https://google.com",
            "https://www.google.com",
    })
    public void shouldThrowBadRequestIfWebUrlLinkIsNotConvertible(String inconvertibleLink) {
        DeepLinkRequest deepLinkRequest = new DeepLinkRequest().setWebUrlLink(inconvertibleLink);
        ResponseEntity<ErrorResponse> deepLinkResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/deep-links",
                deepLinkRequest,
                ErrorResponse.class);
        assertEquals(deepLinkResponseResponseEntity.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertTrue(deepLinkResponseResponseEntity.getBody().getMessage().contains("link is not convertible"));
    }
}
