package com.trendyol.linkconverter.controller;

import com.trendyol.linkconverter.BaseIntegrationTest;
import com.trendyol.linkconverter.dto.ErrorResponse;
import com.trendyol.linkconverter.dto.weburl.WebUrlRequest;
import com.trendyol.linkconverter.dto.weburl.WebUrlResponse;
import com.trendyol.linkconverter.dummy.ProductPageLinkArgumentsProvider;
import com.trendyol.linkconverter.dummy.OtherPageWebUrlArgumentsProvider;
import com.trendyol.linkconverter.dummy.SearchPageLinkArgumentsProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WebUrlControllerIntegrationTest extends BaseIntegrationTest {

    @Autowired
    protected TestRestTemplate restTemplate;

    @LocalServerPort
    private int applicationPort;

    @ParameterizedTest
    @ArgumentsSource(ProductPageLinkArgumentsProvider.class)
    public void shouldConvertCorrectProductPagesToWebUrl(String webUrlLink, String deepLink) {
        WebUrlRequest webUrlRequest = new WebUrlRequest().setDeepLink(deepLink);
        ResponseEntity<WebUrlResponse> webUrlResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/web-urls",
                webUrlRequest,
                WebUrlResponse.class);
        assertEquals(webUrlResponseResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(webUrlResponseResponseEntity.getBody().getWebUrl(), webUrlLink);
    }

    @ParameterizedTest
    @ArgumentsSource(SearchPageLinkArgumentsProvider.class)
    public void shouldConvertCorrectSearchPagesToWebUrl(String webUrlLink, String deepLink) {
        WebUrlRequest webUrlRequest = new WebUrlRequest().setDeepLink(deepLink);
        ResponseEntity<WebUrlResponse> webUrlResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/web-urls",
                webUrlRequest,
                WebUrlResponse.class);
        assertEquals(webUrlResponseResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(webUrlResponseResponseEntity.getBody().getWebUrl(), webUrlLink);
    }

    @ParameterizedTest
    @ArgumentsSource(OtherPageWebUrlArgumentsProvider.class)
    public void shouldConvertCorrectOtherPagesToWebUrl(String webUrlLink, String deepLink) {
        WebUrlRequest webUrlRequest = new WebUrlRequest().setDeepLink(deepLink);
        ResponseEntity<WebUrlResponse> webUrlResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/web-urls",
                webUrlRequest,
                WebUrlResponse.class);
        assertEquals(webUrlResponseResponseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(webUrlResponseResponseEntity.getBody().getWebUrl(), webUrlLink);
    }

    @Test
    public void shouldThrowBadRequestIfRequestBodyIsEmpty() {
        WebUrlRequest webUrlRequest = new WebUrlRequest();
        ResponseEntity<ErrorResponse> errorResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/web-urls",
                webUrlRequest,
                ErrorResponse.class);
        assertEquals(errorResponseResponseEntity.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertTrue(errorResponseResponseEntity.getBody().getMessage().contains("Validation"));
    }

    @Test
    public void shouldThrowBadRequestIfDeepLinkIsEmpty() {
        WebUrlRequest webUrlRequest = new WebUrlRequest().setDeepLink("");
        ResponseEntity<ErrorResponse> errorResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/web-urls",
                webUrlRequest,
                ErrorResponse.class);
        assertEquals(errorResponseResponseEntity.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertTrue(errorResponseResponseEntity.getBody().getMessage().contains("Validation"));
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "ty1://Page",
            "ty://Pag",
            "ty1://",
            "gg://",
    })
    public void shouldThrowBadRequestIfDeepLinkIsNotConvertible(String inconvertibleLink) {
        WebUrlRequest webUrlRequest = new WebUrlRequest().setDeepLink(inconvertibleLink);
        ResponseEntity<ErrorResponse> errorResponseResponseEntity = restTemplate.postForEntity(
                "http://localhost:" + applicationPort + "/api/web-urls",
                webUrlRequest,
                ErrorResponse.class);
        assertEquals(errorResponseResponseEntity.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertTrue(errorResponseResponseEntity.getBody().getMessage().contains("link is not convertible"));
    }
}
