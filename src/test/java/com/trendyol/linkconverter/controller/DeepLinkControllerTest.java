package com.trendyol.linkconverter.controller;

import com.trendyol.linkconverter.dto.deeplink.DeepLinkRequest;
import com.trendyol.linkconverter.exception.ApplicableResolverNotFound;
import com.trendyol.linkconverter.service.LinkService;
import com.trendyol.linkconverter.util.JsonUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(DeepLinkController.class)
public class DeepLinkControllerTest {

    private static final String WEB_URL_LINK = "https://www.trendyol.com/";
    private static final String DEEP_LINK = "ty://?Page=Home";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LinkService linkService;

    @Test
    public void shouldConvertCorrectDeepLink() throws Exception {
        DeepLinkRequest deepLinkRequest = new DeepLinkRequest().setWebUrlLink(WEB_URL_LINK);
        when(linkService.convert(any(String.class))).thenReturn(DEEP_LINK);

        mockMvc.perform(post("/api/deep-links")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.convertToJson(deepLinkRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deepLink", is(DEEP_LINK)));
    }

    @Test
    public void shouldFailIfWebUrlIsNotInRequestBody() throws Exception {
        mockMvc.perform(post("/api/deep-links")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException));
    }

    @Test
    public void shouldFailIfWebUrlIsNotConvertible() throws Exception {
        DeepLinkRequest deepLinkRequest = new DeepLinkRequest().setWebUrlLink(WEB_URL_LINK);
        when(linkService.convert(any(String.class))).thenThrow(new ApplicableResolverNotFound());
        mockMvc.perform(post("/api/deep-links")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.convertToJson(deepLinkRequest)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ApplicableResolverNotFound));
    }
}
