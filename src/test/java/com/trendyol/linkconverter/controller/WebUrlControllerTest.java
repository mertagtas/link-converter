package com.trendyol.linkconverter.controller;

import com.trendyol.linkconverter.dto.weburl.WebUrlRequest;
import com.trendyol.linkconverter.exception.ApplicableResolverNotFound;
import com.trendyol.linkconverter.service.LinkService;
import com.trendyol.linkconverter.util.JsonUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(WebUrlController.class)
public class WebUrlControllerTest {

    private static final String WEB_URL_LINK = "https://www.trendyol.com/";
    private static final String DEEP_LINK = "ty://?Page=Home";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LinkService linkService;

    @Test
    public void shouldConvertCorrectWebUrl() throws Exception {
        WebUrlRequest webUrlRequest = new WebUrlRequest().setDeepLink(DEEP_LINK);
        when(linkService.convert(any(String.class))).thenReturn(WEB_URL_LINK);

        mockMvc.perform(post("/api/web-urls")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.convertToJson(webUrlRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.webUrl", is(WEB_URL_LINK)));
    }

    @Test
    public void shouldFailIfDeepLinkIsNotInRequestBody() throws Exception {
        mockMvc.perform(post("/api/web-urls")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException));
    }

    @Test
    public void shouldFailIfDeepLinkIsNotConvertible() throws Exception {
        WebUrlRequest webUrlRequest = new WebUrlRequest().setDeepLink(DEEP_LINK);
        when(linkService.convert(any(String.class))).thenThrow(new ApplicableResolverNotFound());
        mockMvc.perform(post("/api/web-urls")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.convertToJson(webUrlRequest)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ApplicableResolverNotFound));
    }
}
