package com.trendyol.linkconverter;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.DockerComposeContainer;

import java.io.File;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testcontainers.containers.wait.strategy.Wait.forLogMessage;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BaseIntegrationTest {
    private static final File DOCKER_COMPOSE_FILE = new File(BaseIntegrationTest.class
            .getResource("/docker/docker-compose-integration-test.yml")
            .getFile()
    );

    protected static final DockerComposeContainer<?> environment;

    static {
        environment = new DockerComposeContainer<>(DOCKER_COMPOSE_FILE)
                .withLocalCompose(true)
                .waitingFor("db", forLogMessage("^.*/entrypoint.sh couchbase-server.*$", 1).withStartupTimeout(Duration.ofMinutes(3L)));
        environment.start();
    }

    @Test
    public void checkIfCouchBaseIsReachable() {
        assertTrue(environment.getContainerByServiceName("db_1")
                .map(containerState -> containerState.isRunning() && containerState.isCreated())
                .orElse(false));
    }

}
